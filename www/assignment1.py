#!flask/bin/python

from flask import  Flask, jsonify, json, request, abort, make_response
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import sqlalchemy

app = Flask(__name__)

#Configure MySQL connection
app.config['SQLALCHEMY_DATABASE_URI'] =  'mysql://root:Purplewings@db/lab1_272'
db = SQLAlchemy(app)

# create a class which has same name as table and defines structure of table

class Expense(db.Model):
	#__tablename__ = 'Expense'
	id = db.Column('id', db.Integer, primary_key=True, nullable=False)
	name = db.Column('name', db.String(20), nullable=False)
	email = db.Column('email', db.String(50), nullable=False)
	category = db.Column('category', db.String(50), nullable=False)
	description = db.Column('description', db.String(50), nullable=False)
	link = db.Column('link', db.String(50), nullable=False)
	estimated_costs = db.Column('estimated_costs', db.String(10), nullable=False)
	submit_date = db.Column('submit_date', db.String(10), nullable=False)
	status = db.Column('status', db.String(20))
	decision_date = db.Column('decision_date', db.String(10))

def __init__(self, name, email, category, description, link, estimated_costs, submit_date, status, decision_date):
	#initilize columns
	self.name = name
	self.email = email
	self.category = category
	self.description = description
	self.link = link
	self.estimated_costs = estimated_costs
	self.submit_date = submit_date
	self.status = status
	self.decision_date = decision_date

def __repr__(self):
	return '<Expense %s>' % self.name


class CreateDB():
	def __init__(self):
		#if hostname != None:
		#	HOSTNAME = hostname
		engine = sqlalchemy.create_engine('mysql://root:Purplewings@db')
		engine.execute('CREATE DATABASE IF NOT EXISTS lab1_272')


@app.route('/GET/expenses', methods=['GET'])
def getExpenses(id):
	expenses = Expense.query.all()
	expenses_dict = []
	for e in expenses:
		expenses_dict.append(['name:', e.name, e.email, e.category, e.description, e.link, str(e.estimated_costs), str(e.submit_date)])

	return jsonify(expenses_dict)
#	return jsonify(str(expenses_dict)) #doesnt work

@app.route('/v1/expenses/<int:id>', methods=['GET'])
def getExpensesDetail(id):
        expenses = Expense.query.get(id)
        if expenses is None:
            abort(404)
       	return jsonify(id=str(expenses.id),name=expenses.name,email=expenses.email,category=expenses.category,\
			description=expenses.description,link=expenses.link,estimated_costs=str(expenses.estimated_costs),\
			submit_date=str(expenses.submit_date),status=expenses.status,\
			decision_date=str(expenses.decision_date))

@app.route('/v1/expenses', methods=['POST'])
def createExpenseEntry():
	#if request.method == 'POST':
	# fetch name and rate from the request
	data = json.loads(request.data)
	name = data['name']
	email = data['email']
	category = data['category']
	description = data['description']
	link = data['link']
	estimated_costs = data['estimated_costs']
	submit_date = data['submit_date']
	status = 'pending'

#prepare query statement
	expense = Expense(name=name, email=email, category=category, description=description, link=link, estimated_costs=estimated_costs, submit_date=submit_date,status=status,decision_date='') 


	curr_session = db.session #open database session
	try:
		curr_session.add(expense) #add prepared statment to opened session
        	curr_session.commit() #commit changes
	except:
		curr_session.rollback()
        	curr_session.flush() # for resetting non-commited .add()
	expenseId = expense.id #fetch last inserted id
	data = Expense.query.get(expenseId)
#	Expense.query.filter_by(id=expenseId).first() #fetch our inserted product
#	result = [data.name, data.email] #prepare visual data
#	return jsonify(output=result)

	return jsonify(id=str(data.id),name=data.name,email=data.email,category=data.category,\
	 	description=data.description,link=data.link,estimated_costs=str(data.estimated_costs),\
	 	submit_date=str(data.submit_date),status=data.status,\
         	decision_date=str(data.decision_date)), 201

@app.route('/v1/expenses/<int:id>', methods=['PUT'])
def updateCost(id):
	expenses = Expense.query.get(id)
   	#expenses.estimated_costs = request.json.get('estimated_costs', '')
	expensesUp = json.loads(request.data)
	expenses.estimated_costs = expensesUp['estimated_costs']
	curr_session = db.session #open database session    
	try:
        	curr_session.commit() #commit changes
	except:
		curr_session.rollback()
        curr_session.flush() # for resetting non-commited .add()
	
	return '', 202

@app.route('/v1/expenses/<int:id>', methods=['DELETE'])
def deleteExpense(id):
        expenses = Expense.query.get(id)
        db.session.delete(expenses)
        db.session.commit()
        return '', 204

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not Found'}),404)

@app.route('/createtable')
def createTable():
    db.create_all()
    return json.dumps({'status':True})


@app.route('/createdatabase')
def createDb():
    database = CreateDB()
    return json.dumps({'status':True})

if __name__ == '__main__':
	app.run(debug=True, host="0.0.0.0")
